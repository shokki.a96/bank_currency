<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SimpleXMLElement;
use App\Models\CurrencyRate;

class CurrencyController extends Controller
{
    public function getCurrency()
    {
        // Проверяем, есть ли уже данные в БД
        $cachedData = $this->getCachedData();

        if ($cachedData) {
            return response()->json($cachedData);
        }

        // Если данных нет, обращаемся к внешнему API
        $response = $this->fetchDataFromExternalAPI();

        if ($response->successful()) {
            $data = $this->parseData($response->body());
            $this->cacheData($data);

            return response()->json($data);
        }

        return response()->json(['error' => 'Failed to fetch currency data from external API'], 500);
    }

    private function fetchDataFromExternalAPI()
    {
        return Http::get(config('services.cbr.url'));
    }

    private function parseData($xmlString)
    {
        $xml = new SimpleXMLElement($xmlString);
        $data = [];

        foreach ($xml->Valute as $valute) {
            $data[(string) $valute->CharCode] = (float) str_replace(',', '.', $valute->Value);
        }

        return $data;
    }

    private function cacheData($data)
    {
        foreach ($data as $currencyCode => $rate) {
            CurrencyRate::updateOrCreate(
                ['currency_code' => $currencyCode],
                ['rate' => $rate]
            );
        }
    }

    private function getCachedData()
    {
        $cachedData = CurrencyRate::all()->toArray();
        return $cachedData;
    }
}
